#include <cstdio>
#include "foo.hpp"

extern "C" int rt_init();
extern "C" int rt_term();

int main(void)
{
    rt_init();

    Foo foo = {false, 5, {1, 3, 4}, "test"};

    char* serialize(Foo, bool=false);
    printf("json serialization: %s\n", serialize(foo));
    printf("json serialization: %s\n", serialize(foo, true));

    rt_term();

    return 0;
}
