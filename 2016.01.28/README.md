## How to compile it

1. Install [dmd](http://dlang.org/download.html) on your system
2. Download [cmake-d](https://github.com/dcarp/cmake-d) into `<local_path>`
3. Run following shell commands:

```shell
cd MUCplusplus && mkdir build && cd build
cmake -DCMAKE_MODULE_PATH:PATH=<local_path> ..
make && ./MUCplusplus
```