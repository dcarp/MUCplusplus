#ifndef __FOO_HPP__
#define __FOO_HPP__

struct Foo
{
    bool boolean;
    int integer;
    int integers[10];
    const char* string;
};

#endif
