module jsonserializer;

char* serialize(T)(T value, bool pretty = false)
if (is(T == struct))
{
    import std.array : ElementType;
    import std.conv : to;
    import std.json : JSONValue, toJSON;
    import std.string : fromStringz, toStringz;
    import std.traits : isArray, isPointer, isSomeChar, PointerTarget;

    JSONValue[string] members;

    foreach (member; __traits(allMembers, T))
    {
        alias memberType = typeof(mixin("T." ~ member));

        static if (isArray!memberType && isSomeChar!(ElementType!memberType))
        {
            members[member] = mixin("value." ~ member).ptr.fromStringz.to!string;
        }
        else static if (isPointer!memberType && isSomeChar!(PointerTarget!memberType))
        {
            members[member] = mixin("value." ~ member).fromStringz.to!string;
        }
        else
        {
            members[member] = mixin("value." ~ member);
        }
    }

    JSONValue result = JSONValue(members);

    return cast(char*) toJSON(&result, pretty).toStringz;
}
