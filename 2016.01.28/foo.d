module foo;

struct Foo
{
    bool boolean;
    int integer;
    int[10] integers;
    const(char)* string;
}

extern(C++) char* serialize(Foo value, bool pretty = false)
{
    import jsonserializer : serialize;

    return serialize(value, pretty);
}
